﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NuGet;

namespace NugetDownloader
{
    class Program
    {
        public static readonly string StartLocation = @"Z:\NugetTest\";
        public static string InstallLocation;

        public static readonly IPackageRepository Repo = PackageRepositoryFactory.Default.CreateRepository("https://packages.nuget.org/api/v2");

        static void Main(string[] args)
        {
            // Just keep on going forever and ever
            while (true)
            {
                // First get the package ID they want
                string p = GetPackageIdFromUser();
                // Get the package object for that Id
                IPackage package = ResolvePackageForId(p);
                if (package != null)
                {
                    // Save the package and it's dependencies to nugpk file
                    SaveAndResolveDependencies(package);
                }
            }
        }

        public static string GetPackageIdFromUser()
        {
            string packageId = null;
            while (packageId == null)
            {
                Console.WriteLine("What is the package Id?");
                packageId = Console.ReadLine();

                if (packageId == null || packageId.Trim() == String.Empty)
                {
                    Console.WriteLine("Invalid package name given");
                    packageId = null;
                }
            }
            packageId = packageId.Trim();
            Console.WriteLine("Using package: " + packageId);
            return packageId;
        }

        public static IPackage ResolvePackageForId(string packageId)
        {
            // Add random file name to install loc
            InstallLocation = StartLocation + @"\" + Guid.NewGuid() + @"\";
            // Create the directory
            Directory.CreateDirectory(InstallLocation);
            
            //Get the list of all NuGet packages with ID 'EntityFramework'       
            List<IPackage> packages = Repo.FindPackagesById(packageId).ToList();

            //Iterate through the list and print the full name of the pre-release packages to console
            IPackage selectedPackage = null;
            while (selectedPackage == null)
            {
                int i = 1;
                foreach (var p in packages)
                {
                    Console.WriteLine(i + " => " + p.GetFullName());
                    i++;
                }
                // Get response
                Console.WriteLine("Select the version #: ");
                string response = Console.ReadLine();
                int reponseNo;
                if (int.TryParse(response, out reponseNo))
                {
                    // Is a #, is it the right #?
                    if (--reponseNo < packages.Count())
                    {
                        selectedPackage = packages[reponseNo];
                    }
                }
            }

            Console.WriteLine("Using package " + selectedPackage.GetFullName());
            
            // Return the package they want
            return selectedPackage;
        }

        public static void SaveAndResolveDependencies(IPackage selectedPackage)
        {
            // Start by saving the nugetpk to the file system
            SavePackageToNugpk(selectedPackage);
            // Now do the same for it's dependencies
            foreach (var dependencySet in selectedPackage.DependencySets)
            {
                Console.WriteLine("... Found dependency set");
                foreach (var dependency in dependencySet.Dependencies)
                {
                    Console.WriteLine("...... Found dependency: " + dependency.Id + " " + dependency.VersionSpec);
                    // Get a package file for that ID
                    IPackage depPackage = Repo.ResolveDependency(dependency, true, true);
                    if (depPackage == null)
                    {
                        Console.WriteLine("Error getting dependent package from repo");
                    }
                    else
                    {
                        SavePackageToNugpk(depPackage);
                    }
                }
            }

            // Open the new folder we added file sto
            Process.Start(InstallLocation);
        }

        public static void SavePackageToNugpk(IPackage package)
        {
            var stream = package.GetStream();
            using (var fileStream = File.Create(InstallLocation + package.GetFullName() + ".nugpk"))
            {
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
            }
        }
    }
}
